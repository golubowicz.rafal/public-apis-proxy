export interface ExternalPublicApisListEntry {
  API: string;
  Description: string;
  Auth: string;
  HTTPS: string;
  Cors: string;
  Link: string;
  Category: string;
}

export interface ExternalPublicApisListResponse {
  count: number;
  entries: ExternalPublicApisListEntry[];
}

export interface API {
  title: string;
  description: string;
  link: string;
  category: string;
  cors?: boolean;
}

export interface GetAPIsListResponse {
  items: Array<API>;
}

export interface GetApisListFilters {
  title?: string;
  cors?: string;
}
