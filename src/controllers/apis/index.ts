import { Request, Response } from 'express';

import { fetchPublicApis } from './apisProxy';
import { normalizeBoolean } from '../../utils';
import { GetApisListFilters, GetAPIsListResponse } from './types';

export const getList = async (
  req: Request<unknown, unknown, unknown, GetApisListFilters>,
  res: Response<GetAPIsListResponse>,
) => {
  try {
    const { entries } = await fetchPublicApis(req.query);

    const apisList = entries.map((entry) => ({
      link: entry.Link,
      title: entry.API,
      category: entry.Category,
      description: entry.Description,
      cors: normalizeBoolean(entry.Cors),
    }));

    res.status(200).send({ items: apisList });
  } catch {
    // TODO: Add error handler
    res.sendStatus(404);
  }
};
