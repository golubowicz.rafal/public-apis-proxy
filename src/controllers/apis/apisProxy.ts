import axios from 'axios';

import { normalizeBoolean } from '../../utils';
import { PUBLIC_APIS_ENDPOINT } from '../../setup/environment';
import { ExternalPublicApisListResponse, GetApisListFilters } from './types';

export const fetchPublicApis = async (
  filters: GetApisListFilters,
): Promise<ExternalPublicApisListResponse> => {
  const url = preparePublicApisUrl(filters);

  const { data } = await axios.get<ExternalPublicApisListResponse>(url);

  return data;
};

const preparePublicApisUrl = (filters: GetApisListFilters) => {
  const url = new URL(PUBLIC_APIS_ENDPOINT);

  if (filters.title) {
    url.searchParams.append('title', filters.title);
  }

  // It's to determine if the user passes 'undefined' or that filter has been omitted.
  if ('cors' in filters) {
    url.searchParams.append(
      'cors',
      parseCorsFilter(normalizeBoolean(filters.cors)),
    );
  }

  return url.href;
};

const parseCorsFilter = (cors?: boolean): string => {
  if (!cors) return 'unknown';

  return cors ? 'yes' : 'no';
};
