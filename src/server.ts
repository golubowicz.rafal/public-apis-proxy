import './setup/config';

import cors from 'cors';
import express from 'express';

import { normalizePort } from './utils';
import { CORS_ORIGIN, PORT } from './setup/environment';
import router from './router';

const app = express();

app.use(express.json());
app.use(cors({ origin: CORS_ORIGIN }));

app.use('/', router);

const port = normalizePort(PORT);

app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`);
});
