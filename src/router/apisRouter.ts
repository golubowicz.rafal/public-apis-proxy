import { Router } from 'express';

import { getList } from '../controllers/apis';

const apisRouter = Router();

apisRouter.get('/', getList);

export default apisRouter;
