import { Router } from 'express';

import { API_ROUTES } from './const';
import apisRouter from './apisRouter';

const router = Router();

router.use(API_ROUTES.API_LIST, apisRouter);

export default router;
