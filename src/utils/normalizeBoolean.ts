const normalizeBoolean = (val?: string): boolean | undefined => {
  if (val === 'yes' || val === 'true') return true;
  if (val === 'no' || val === 'false') return false;

  return undefined;
};

export { normalizeBoolean };
