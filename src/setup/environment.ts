const PUBLIC_APIS_ENDPOINT = (() => {
  if (!process.env.PUBLIC_APIS_ENDPOINT) {
    throw new Error('Missing environment variable - PUBLIC_APIS_ENDPOINT');
  }

  return process.env.PUBLIC_APIS_ENDPOINT;
})();

const PORT = process.env.PORT || 8080;
const CORS_ORIGIN = process.env.CORS_ORIGIN;

export { PORT, CORS_ORIGIN, PUBLIC_APIS_ENDPOINT };
