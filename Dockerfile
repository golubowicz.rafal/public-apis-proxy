FROM node:16

WORKDIR /apis-proxy-api

COPY . /apis-proxy-api

RUN yarn

CMD [ "yarn", "dev" ]
